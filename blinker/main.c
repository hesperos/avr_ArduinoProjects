#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include <avr/io.h>
#include <util/delay.h>

#include "pca.h"


void bpwm() {
	static uint32_t x = 0;
	OCR1AL = ( sin(2*3.14*x)*127 ) + 127;
	x += 1;
}


void disco() {
	static uint8_t x = 0;
	OCR1AL = (x++ & 0x01) ? 0xff : 0x00;
}


struct blinker {
	void (*cb)(void);
	uint32_t cycles;
	uint32_t delay_ms;
};


struct blinker blinkers[] = {
	{ bpwm, 8192, 2 },
	{ disco, 64, 50 },
	{ NULL, 0, 0 },
};


int main(void) {

	tpwm_pwm_init(E_TIMER1, E_PWM_SINGLE);
	tpwm_setup_for_bitres(E_TIMER1, 8);
	tdelay_init(E_TIMER0);

	DDRB = 0xff;
	OCR1AH = 0;

	while (1) {
		struct blinker *b = blinkers;
		uint32_t cnt = 0;

		// loop over blinkers
		while (b->cb) {
			if (!cnt) cnt = b->cycles;

			b->cb();
			cnt--;

			tdelay_ms(E_TIMER0, b->delay_ms);
			if (!cnt) b++;
		}
	}

	return 0;
}
