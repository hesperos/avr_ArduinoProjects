#include "main.h"
#include "sump.h"
#include "analyzer.h"

#include <string.h>
#include <util/delay.h>


void main(void) {
	uint8_t input = 0x00;
	volatile struct analyzer a;
	
#if ENABLE_REFERENCE_FREQ == 1
	{
		// reference frequency of 10 kHz generated
		// on PINB3
		uint32_t pocr = 0;
		pocr = _timer_freq_prescale(E_TIMER2, 20000, 255);
		_timer_init_ctc(E_TIMER2);
		_timer_setup_ctc(E_TIMER2, pocr);
		_timer_en_oca(E_TIMER2);
		TCCR2A |= _BV(COM0A0);
	}
#endif

	analyzer_init(&a);

	for (;;) {

		if (!serial_getc(&input)) continue;

#if DEBUG_ENABLED == 1
		printf("Received [%02x]\n", input);
		continue;
#endif

		// interpret the command
		switch(input) {

			case SUMP_ID:
				printf("1ALS");
				fflush(stdout);
				break;

			case SUMP_GET_METADATA:
				sump_provide_metadata(DEVICE_PROBES, 
						DEVICE_STATIC_SAMPLES,
						DEVICE_DYNAMIC_SAMPLES,
						DEVICE_SAMPLE_RATE);
				break;

			case SUMP_RUN:
				analyzer_acquire_data(&a);
				break;

			case SUMP_XON:
				break;

			case SUMP_XOFF:
				break;

			case SUMP_SET_TRIGGER_MASK:
				serial_recv((char *)&a.t.mask, 4, 1);
				break;

			case SUMP_SET_TRIGGER_VALUE:
				serial_recv((char *)&a.t.value, 4, 1);
				break;

			case SUMP_SET_TRIGGER_CONF:
				serial_recv((char *)&a.t.conf.raw, 4, 1);
				break;

			case SUMP_SET_DIVIDER: 
				{
					uint8_t x;
					serial_recv((char *)&a.s.divider, 3, 1);
					// drop the last received byte - it's irrelevant
					serial_recv(&x, 1, 1);
				}
				break;

			case SUMP_SET_READY_DELAY_COUNT:
				serial_recv((char *)&a.s.read_cnt, 2, 1);
				serial_recv((char *)&a.s.delay_cnt, 2, 1);
				a.s.read_cnt = (a.s.read_cnt + 1) << 2;
				a.s.delay_cnt = (a.s.delay_cnt + 1) << 2;
				break;

			case SUMP_SET_FLAGS:
				{
					uint8_t data[4] = {0x00};
					serial_recv(data, 4, 1);
					a.s.flags.raw = data[0];
				}
				break;

			default:
			case SUMP_RESET:
				break;
		}

	}
}
