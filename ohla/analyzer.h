#ifndef SETTINGS_H_CRMGPAY0
#define SETTINGS_H_CRMGPAY0

#include <stdint.h>

#include "defines.h"

struct trigger {
	uint32_t value;
	uint32_t mask;

	union {
		uint32_t raw;
		struct {
			uint8_t chan4 : 1;
			uint8_t res1 : 1;
			uint8_t serial : 1;
			uint8_t start : 1;
			uint8_t res2 : 4;
			uint8_t level : 2;
			uint8_t res3 : 2;
			uint8_t chan03 : 4;
			uint16_t delay;
		} c;
	} conf;
};


struct settings {
	uint32_t divider;
	volatile uint16_t delay_cnt;
	volatile uint16_t read_cnt;

	union {
		uint8_t raw;
		struct {
			uint8_t demux : 1;
			uint8_t filter : 1;
			uint8_t ch_groups : 4;
			uint8_t external_clk : 1;
			uint8_t invert_clk : 1;
		} bits;
	} flags;
};


struct analyzer {
	volatile struct settings s;
	struct trigger t;
	volatile uint8_t data[DEVICE_STATIC_SAMPLES];
	volatile int16_t rh,rt;
	volatile uint8_t mh,mt;
};

void analyzer_init(volatile struct analyzer *a);
void analyzer_acquire_data(volatile struct analyzer *a);
void analyzer_transfer_data(volatile struct analyzer *a);

#endif /* end of include guard: SETTINGS_H_CRMGPAY0 */

