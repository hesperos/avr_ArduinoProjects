#include <avr/interrupt.h>
#include "pca.h"
#include "analyzer.h"
#include "defines.h"

static volatile uint8_t *data = NULL;
static volatile int16_t h;
static volatile int16_t t;
register uint8_t mh asm("r2");
register uint8_t mt asm("r3");
static volatile int16_t cnt = 0;


void inc_ptr(volatile int16_t *p, volatile uint8_t *m) {
	(*p)++;
	if ((*p) == 1024) {
		(*p) = 0;
		(*m) ^= 0x01;
	}
}


ISR(TIMER1_COMPA_vect) {
	data[h] = DEVICE_CAPTURE_PORT;

	// buffer full
	/* if ((h == t) && (mh != mt)) { */
		/* t++; */
		/* if (t == 1024) { */
		/* 	t = 0; */
		/* 	mt ^= 1; */
		/* } */
		/* inc_ptr(&t, &mt); */
	/* } */

	/* inc_ptr(&h, &mh); */
	h++;
	if (h == 1024) {
		h = 0;
		mh ^= 1;
	}
	--cnt;
}


void analyzer_irq_fire(volatile struct analyzer *a) {
	data = a->data;

	h = a->rh;
	t = a->rt;
	mh = a->mh;
	mt = a->mt;

	cnt = a->s.delay_cnt;
	_timer_en_compa_int(E_TIMER1);
}


void analyzer_wait(volatile struct analyzer *a) {
	while (cnt > 0);

	a->rh = h;
	a->rt = t;
	a->mh = mh;
	a->mt = mt;

	_timer_dis_compa_int(E_TIMER1);
}
