#ifndef __IRQ_H__
#define __IRQ_H__

#include "analyzer.h"

void analyzer_irq_fire(volatile struct analyzer *a);
void inc_ptr(volatile int16_t *p, volatile uint8_t *m);


#endif /* __IRQ_H__ */
