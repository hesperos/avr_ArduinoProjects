#include "analyzer.h"
#include "pca.h"
#include "irq.h"

#include <string.h>


void analyzer_init(volatile struct analyzer *a) {
	// initialize serial port
	serial_init(SERIAL_SPEED);	
	serial_install_interrupts(E_FLAGS_SERIAL_RX_INTERRUPT);
	serial_install_stdio();
	serial_flush();

	memset((void *)a, 0x00, sizeof(struct analyzer));
	a->s.read_cnt = DEVICE_READ_COUNT;
	a->s.delay_cnt = DEVICE_DELAY_COUNT;

	_timer_init_ctc(E_TIMER1);

	// data port
	DDRC = 0x00;
}

#define FREQ_100K 100000
#define FREQ_200K (2 * FREQ_100K)
#define FREQ_500K (5 * FREQ_100K)
#define FREQ_1M 1000000
#define FREQ_2M (2 * FREQ_1M)
#define FREQ_4M (4 * FREQ_1M)
#define FREQ_100M (100 * FREQ_1M)

void analyzer_acquire_data(volatile struct analyzer *a) {
	uint32_t freq = (uint32_t)((double)(FREQ_100M)/ (a->s.divider + 1));

	a->rh = a->rt;
	a->mh = a->mt;

	/**
	 * @brief for low frequencies use the timer
	 * For high frequencies a specialized functions must be used
	 */
	if (freq <= 100000) {
		// for this freq range use the timer to collect data
		uint32_t pocr = 0;
		pocr = _timer_freq_prescale(E_TIMER1, freq, 65535);
		_timer_setup_ctc(E_TIMER1, pocr);
		analyzer_irq_fire(a);
		analyzer_wait(a);
	}
	else {
		switch (freq) {
			case FREQ_100K:
				break;

			case FREQ_200K:
				break;

			case FREQ_500K:
				break;

			case FREQ_1M:
				break;

			case FREQ_2M:
				break;

			case FREQ_4M:
				break;

			default:
				break;
		}
	}

	analyzer_transfer_data(a);
}


void analyzer_transfer_data(volatile struct analyzer *a) {
	while (!(a->rh == a->rt && a->mh == a->mt) && a->s.read_cnt) {
		serial_poll_sendc(a->data[a->rt]);
		inc_ptr(&a->rt, &a->mt);
		a->s.read_cnt--;
	}
}

/* void acquire_data(struct analyzer *a) { */
/* 	 */
/* 	uint16_t i = 0; */
/* 	uint32_t x = 16; */
/*  */
/* 	for (i = 0; i < a->s.read_cnt; i++) { */
/* 		a->data[i] = PINC; */
/* 		x = 8192; */
/* 		while(x) x--; */
/* 	} */
/* 	 */
/* 	serial_poll_send(a->data, a->s.read_cnt); */
/* } */


