#ifndef __SETUP_H__
#define __SETUP_H__

#include <stdint.h>

void setup_pwm(uint32_t a_pwm_freq);

#endif /* __SETUP_H__ */
