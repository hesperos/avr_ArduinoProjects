#ifndef CONSTANTS_H_Y1S6LOEZ
#define CONSTANTS_H_Y1S6LOEZ

#define F_KHZ 1000
#define PWM_FREQUENCY (8*F_KHZ)

#endif /* end of include guard: CONSTANTS_H_Y1S6LOEZ */
