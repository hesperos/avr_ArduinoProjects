avr_ArduinoProjects
===================

Repository for Arduino Related projects

- binary_slip - SLIP line synchronization test project
- blinker - programmable blinker
- cli_mcu - simple CLI system
- clk - Hitachi display clock
- d7seg_thermometer - 7seg display thermometer
- dac - audio dac with PWM
- ledmatrix_disp - multiplexed led matrix display
- ledmatrix_marquee - multiplexed led matrix display
- musicbox - arduino as MIDI music box
- nokia_spi - example of using the nokia LCD
- serial_stdio - test of serial stdio driver
- slip_crc - SLIP crc test project
- soft_rtc - soft RTC using timers
- sudoku - serial sudoku solver


Work in Progress:
(Unfinished or experimental):
- synth - soft synth
- ohla - open hardware logic analyzer
- uscope - analog scope with nokia lcd
