#ifndef __MAIN_H__
#define __MAIN_H__

#define SAMPLES_BUFF_SIZE 86


/**
 * @brief determine next head position
 */
#define SAMPLES_NEXT_HEAD(__buff) \
	((__buff.s.r.head + 1) % SAMPLES_BUFF_SIZE)

#define SAMPLES_NEXT_TAIL(__buff) \
	((__buff.s.r.tail + 1) % SAMPLES_BUFF_SIZE)

#define SAMPLES_FULL(__buff) \
	(__buff.next == __buff.s.r.tail)

#define SAMPLES_NOT_FULL(__buff) \
	(__buff.next != __buff.s.r.tail)


/**
 * @brief samples buffer
 */
struct samples {
	union {
		volatile uint8_t raw[SAMPLES_BUFF_SIZE + RING_SIZE];
		volatile ring_buffer r;
	} s;

	volatile uint8_t next;
};


#endif /* __MAIN_H__ */
