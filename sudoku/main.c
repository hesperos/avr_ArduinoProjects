#include "board.h"
#include "csv_reader.h"
#include "bt_solver.h"

#include <pca.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>


int main()
{
	board_t board;
	char board_file[128] = {0x00};
	uint8_t intermediate = 0;
	unsigned _delay = 0;

	serial_init(E_BAUD_4800);	
	serial_install_interrupts(E_FLAGS_SERIAL_RX_INTERRUPT);
	serial_flush();

	serial_install_stdio();

	while (1) {
		board_init(board);
		csv_load_stdin(board);

		printf("board data received, solving...\n");
		bt_solve(board, _delay, intermediate ? board_print : NULL);

		if (!intermediate) board_print(board);
	}

	return 0;
}
