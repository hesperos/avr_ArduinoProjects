#ifndef BT_SOLVER_H_ESLD7CET
#define BT_SOLVER_H_ESLD7CET

#include "board.h"

typedef void (*pcb_t)(board_t board);

uint8_t bt_solve(board_t board, unsigned int delay, pcb_t cb);

#endif /* end of include guard: BT_SOLVER_H_ESLD7CET */
