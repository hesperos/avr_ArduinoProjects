#ifndef CSV_READER_H_8QPSHRU0
#define CSV_READER_H_8QPSHRU0

#include "board.h"


int8_t csv_load(board_t board, const char* a_f);
int8_t csv_load_stdin(board_t board);


#endif /* end of include guard: CSV_READER_H_8QPSHRU0 */
