#include "csv_reader.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>


int8_t csv_load(board_t board, const char* a_f) {
	FILE *f = NULL;
	char line[128] = {0x00};
	int n = 0;
	
	if ((f = fopen(a_f, "r")) == NULL) {
		return -1;
	}

	while ((n < EDGE_SIZE) ) {
		int m = 0;
		char *ptr = NULL;

		fgets(line, sizeof(line), stdin);
		ptr = strtok(line, ",");
		printf("got line: %d\n", n);

		while(ptr) {
			board[ board_idx(m, n) ] = atoi(ptr);
			ptr = strtok(NULL, ",");
			m++;
		}
		n++;
	}
	
	fclose(f);
	return 0;
}


int8_t csv_load_stdin(board_t board) {
	char line[128] = {0x00};
	int n = 0;

	printf("awaitening board data...\n");

	while ((n < EDGE_SIZE)) {
		int m = 0;
		char *ptr = NULL;

		scanf("%s", line);
		ptr = strtok(line, ",");

		while(ptr) {
			board[ board_idx(m, n) ] = atoi(ptr);
			ptr = strtok(NULL, ",");
			m++;
		}
		n++;
	}

	return 0;
}
