#ifndef BOARD_H_9OIUK2FG
#define BOARD_H_9OIUK2FG

#include <stdint.h>

 /*
  *  Coordinates...
  *
  *     X
  *   +------->
  *   |  
  * Y |   +-------+----
  *   |   |       |
  *   v   |       |
  *       |       |
  *       +-------+----
  *       |       |
  *       |       |
  *       |       |
  */


/**
 * @brief board box size
 */
#define SIZE 3


/**
 * @brief size of the board edge
 */
#define EDGE_SIZE (SIZE*SIZE)


/**
 * @brief field value type
 */
typedef uint8_t value_t;


/**
 * @brief board type definition
 */
typedef value_t board_t[EDGE_SIZE * EDGE_SIZE];


void board_init(board_t board);
void board_print(board_t board);
uint8_t board_idx(uint8_t x, uint8_t y);
uint8_t board_check_col(board_t board, uint8_t x, value_t v);
uint8_t board_check_row(board_t board, uint8_t y, value_t v);
uint8_t board_check_box(board_t board, uint8_t x, uint8_t y, value_t v);
uint8_t board_find_empty(board_t board, uint8_t* x, uint8_t* y);


#endif /* end of include guard: BOARD_H_9OIUK2FG */
