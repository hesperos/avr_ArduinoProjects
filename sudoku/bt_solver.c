#include "bt_solver.h"

#define _BSD_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>


static unsigned int g_cnt = 0;
static pcb_t g_cb = NULL;


static uint8_t _bt_solve(board_t board) {
	uint8_t nx = 0;
	uint8_t ny = 0;

	g_cnt++;
	if (!board_find_empty(board, &nx, &ny)) {
		printf("board full\n");
		return 1;
	}
	
	for (int v = 1; v<=9; v++) {
		if (board_check_row(board, ny, v) ||
				board_check_col(board, nx,v) ||
				board_check_box(board, nx,ny,v)) {
			continue;
		}

		// move is valid
		board[ board_idx(nx,ny) ] = v;
		if (g_cb) g_cb(board);

		if (_bt_solve(board)) return 1;

		board[ board_idx(nx,ny) ] = 0;
		if (g_cb) g_cb(board);
	}

	return 0;
}


uint8_t bt_solve(board_t board, unsigned int delay, pcb_t cb) {
	uint8_t r = 0;

	g_cnt = 0;
	g_cb = cb;
	r = _bt_solve(board);

	printf("(%d)cnt: %u\n", r, g_cnt);
	return r;
}

