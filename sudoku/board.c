#include "board.h"

#include <string.h>
#include <stdio.h>

/* ================================================================================ */

static uint8_t _find(board_t board, uint8_t* x, uint8_t* y, value_t v);
static void _hline();

/* ================================================================================ */

void board_init(board_t board) {
	memset(board, 0x00, sizeof(board_t));
}


uint8_t board_check_col(board_t board, uint8_t x, value_t v) {
	for (int y = 0; y < EDGE_SIZE; y++) {
		if (board[ board_idx(x,y) ] == v) return 1;
	}
	return 0;
}


uint8_t board_check_row(board_t board, uint8_t y, value_t v) {
	for (int x = 0; x < EDGE_SIZE; x++) {
		if (board[board_idx(x, y)] == v) return 1;
	}
	return 0;
}


uint8_t board_check_box(board_t board, uint8_t x, uint8_t y, value_t v) {
	uint8_t bx = x - (x%SIZE);
	uint8_t by = y - (y%SIZE);
	for (int i = 0; i < SIZE; i++) {
		for (int j = 0; j < SIZE; j++) {
			if (board[board_idx(i + bx,j + by)] == v) return 1;
		}
	}
	return 0;
}


uint8_t board_find_empty(board_t board, uint8_t* x, uint8_t* y) {
	return _find(board, x, y, 0);
}


uint8_t board_idx(uint8_t x, uint8_t y) {
	return (x + ( y*EDGE_SIZE ));
}


void board_print(board_t board) {
	for (uint8_t y = 0; y < EDGE_SIZE; y++) {
		if (!(y%SIZE)) _hline();
		for (uint8_t x = 0; x < EDGE_SIZE; x++) {
			int v = board[ board_idx(x,y) ];
			printf("%s", x%SIZE ? " " : "| ");
			if (v)
				printf("%d ", v);
			else
				printf("  ");
		}
		printf("\n");
	}
	_hline();
}

/* ================================================================================ */

static void _hline() {
	for (unsigned i = 0; i < (EDGE_SIZE*3 + 2); i++) printf("=");
	printf("\n");
}


static uint8_t _find(board_t board, uint8_t* x, uint8_t* y, value_t v) {
	int i = 0, j = 0;
	for (j = 0; j < EDGE_SIZE; j++) {
		for (i = 0; i < EDGE_SIZE; i++) {
			if (board[ board_idx(i,j) ] == v) {
				*x = i;
				*y = j;
				return 1;
			} // if
		} // for
	} // for

	return 0;
}
/* ================================================================================ */
